from requests import post
from utils import (
    extract_url,
)


class SaveToGraphDatabase:
    endpoint: str = 'https://nameless-brook-320072.eu-central-1.aws.cloud.dgraph.io/graphql'
    query: str = """
    mutation ($page:[AddPageInput!]!) {
        addPage(input: $page) { numUids }
    }
    """

    def open_spider(self, spider):
        self.spider = spider

    def process_item(self, item: dict, spider) -> dict:

        page: dict = {
            'url': item['url'],
            'h1': item['h1'],
            'title': item['title'],
            'links': [{'url': url} for url in item['links']],
            'suffix': item['suffix'],
            'redirects': item['redirects'],
            'root_url': item['root_url'],
            'download_latency': item['download_latency'],
            'https': item['https'],
            'n_external_links': item['n_external_links'],
            'n_internal_links': item['n_internal_links'],
        }

        r = post(self.endpoint, json={'query': self.query, 'variables': {'page': page}})
        self.spider.logger.debug(f'{r.status_code} {r.content.decode()}')
        return item


class Average_links:
    internal_links: list = []
    external_links: list = []

    def open_spider(self, spider) -> None:
        self.spider = spider

    def close_spider(self, spider) -> None:
        avg_internal_links = sum(self.internal_links) / len(self.internal_links)
        avg_external_links = sum(self.external_links) / len(self.external_links)
        spider.logger.info(f'Avg internal links: {avg_internal_links}')
        spider.logger.info(f'Avg external links: {avg_external_links}')

    def process_item(self, item: dict, spider) -> dict:
        self.internal_links.append(item['n_internal_links'])
        self.external_links.append(item['n_external_links'])
        return item


class PageDifficultToEnter:
    all_pages: dict = {}

    def open_spider(self, spider):
        self.spider = spider

    def close_spider(self, spider):

        spider.logger.info('5 least linked pages:')
        for _ in range(5):
            url: str = min(self.all_pages, key=self.all_pages.get)  # type: ignore
            spider.logger.info(f' - {url}: {self.all_pages.pop(url)}')

        spider.logger.info('5 most linked pages:')
        for _ in range(5):
            url: str = max(self.all_pages, key=self.all_pages.get)  # type: ignore
            spider.logger.info(f' - {url}: {self.all_pages.pop(url)}')

    def process_item(self, item: dict, spider) -> dict:

        for link in item['links']:
            root_url, suffix, subdomain = extract_url(link)
            if root_url == 'globalapptesting.com':
                self.all_pages.setdefault(link, 0)
                self.all_pages[link] += 1

        return item
