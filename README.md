## Install
```
python3 -m pip install -qr requirements.txt
```

## Run crawler & scrapper
```
python3 run.py
```

## Files structure
- run.py - run spider
- spider.py - Class responsible for crawler and scrap website
- pipelines.py - data processing from spider
- settings.py - config dict

- graph_sql_example.py - GraphQL code send one page data to Dgraph cloud
- tox.ini - dev configs
- utils.py - helpers

## Requirements:
- to see any possible transtions between subpages login to https://cloud.dgraph.io/_/data and provide me your email to
  share project
- as map representation is used graph database
- the average number of links coming out of subpages of our website to different websites: 49.8 or 41 (there are two solution
  used - one python code in pipelines.Average_links, second aggregate query of Dgraph)
- the the average number of internal links on our website: 11.8 or 6.5 (there are two solution used - one python code in pipelines.Average_links, second aggregate query of Dgraph)
-
- the dead link - https://www.globalapptesting.com?hsLang=en
- the most difficult pages to enter 

```
    - https://go.globalapptesting.com/press/iso-27001-certification: 1
    - https://go.globalapptesting.com/partnership: 1
    - https://status.globalapptesting.com/: 1
    - https://go.globalapptesting.com/hubfs/Marketing/content/Case%20Study/LiveSafe%20-%20Case%20Study.pdf: 1
    - https://go.globalapptesting.com/en/how-to-increase-the-success-of-your-business-with-quality-recording: 1
```

- the most linked pages

```
    - https://www.globalapptesting.com/platform/integrations: 324
    - https://www.globalapptesting.com/solutions/usability-testing: 324
    - https://www.globalapptesting.com/product: 324
    - https://www.globalapptesting.com/solutions: 324
    - https://www.globalapptesting.com/code-of-conduct: 324
```

- to see graphical representation of the subpages of our website and their relations
I'd like to use Ratel (however, due to lack of time, I did not finish it)
- https://dgraph.io/docs/ratel/overview/
- https://d25jw0bj0s58lg.cloudfront.net/optimized/2X/f/f5f070b795163159a7b60a7d61b45429a8459d99_2_690x477.png
