from datetime import datetime

from scrapy import Request, Spider
from scrapy.spidermiddlewares.httperror import HttpError
from settings import settings
from twisted.internet.error import DNSLookupError
from twisted.internet.error import TimeoutError
from utils import (
    extract_url,
    optymize_text,
)


class SpiderGlobalTesting(Spider):
    custom_settings = settings
    spider_id = None
    allowed_domains: list = ['globalapptesting.com']
    blacklist: set = {
        'http://127.0.0.1',
        '127.0.0.1',
        'http://192.168.1.1',
        '192.168.1.1'
    }
    data: dict = {  # used for a final summary
        'stats': {},
        'hostname': 'host',
        'version': '0.0.1',
        'start': datetime.now(),
        'saved': 0
    }
    errors: dict = {  # used for a final summary
        'ALL': 0,
        'DNS': 0,
        'Time': 0,
        'Duplicated': 0,
        'Filtered': 0,
        'HttpError': 0,
        'data': {
            'DNS': [],
            'Time': [],
            'Duplicated': [],
            'HttpError': [],
            'others': []
        }
    }

    def __init__(self, name=None, **kwargs):
        custom_settings = kwargs.get('settings', settings)
        self.start_urls = custom_settings['START_URLS']

    def start_requests(self):
        for url in self.start_urls:
            yield Request(url, callback=self.parse, errback=self.errback)

    def _count_external_links(self, links):
        count: int = 0
        for link in links:
            root_url, suffix, subdomain = extract_url(link)
            if root_url == self.custom_settings['SITE_GLOBAL_TESTING']:
                count += 1

        return count

    def parse(self, r):
        try:
            redirects = r.meta.get('redirect_times', 0)
            root_url, suffix, subdomain = extract_url(r.url)
            self.blacklist.add(r.url)
            print(f'{r.url}')
            links = r.xpath("//a[contains(@href,'http')]/@href").getall()
            links = {link for link in links if 'mailto' not in link or 'pdf' not in link}
            n_external_links = self._count_external_links(links)
            n_internal_links = len(links) - n_external_links

            yield {
                'url': r.url,
                'https': bool(r.certificate),
                'title': optymize_text(r.xpath('//title/text()').get(None)),
                'h1': optymize_text(r.xpath('//h1/text()').get(None)),
                'download_latency': r.meta['download_latency'],
                'root_url': root_url,
                'suffix': suffix,
                'redirects': redirects,
                'download_slot': r.meta['download_slot'].strip(),
                'links': links,
                'spider_id': self.spider_id,
                'version': self.data['version'],
                'n_external_links': n_external_links,
                'n_internal_links': n_internal_links,
            }

            for next_page in links:
                if next_page not in self.blacklist:
                    yield r.follow(next_page, self.parse)

        except Exception as e:
            self.logger.warning(f'{e}, {r.url}')

    def closed(self, reason):
        self.logger.info(reason)

    def errback(self, failure):
        request = failure.request

        if failure.check(HttpError):  # if isinstance(failure.value, HttpError):
            self.logger.debug(f'Http {request.url}')
            self.errors['data']['HttpError'].append(request.url)
            self.errors['HttpError'] += 1

        elif failure.check(DNSLookupError):  # elif isinstance(failure.value, DNSLookupError):
            self.logger.debug(f'DNS: {request.url}')
            self.errors['data']['DNS'].append(request.url)
            self.errors['DNS'] += 1

        elif failure.check(TimeoutError):  # elif isinstance(failure.value, TimeoutError):
            self.logger.debug(f'Timeout: {request.url}')
            self.errors['data']['Time'].append(request.url)
            self.errors['Time'] += 1
        else:
            self.errors['data']['others'].append(request.url)

        self.errors['ALL'] += 1
