from tldextract import extract


def extract_url(url: str = '') -> tuple:
    extracted_url = extract(str(url))
    if extracted_url.domain and extracted_url.suffix:
        return f'{extracted_url.domain}.{extracted_url.suffix}', extracted_url.suffix, extracted_url.subdomain
    return (None, None, None)


def optymize_text(text=None):
    if text is None:
        return None
    text = [c for c in text if c.isalnum() or c.isspace()]
    return (''.join(text).replace('  ', ' ').strip())[:90]
