from requests import post


page: dict = {
    'url': 'test35.om',
    'h1': 'Test',
    'title': 'Test',
    'links': [
        {'url': 'url.com'},
    ]
}

query = """
mutation ($page:[AddPageInput!]!) {
    addPage(input: $page) { numUids }
}
"""


URL: str = 'https://nameless-brook-320072.eu-central-1.aws.cloud.dgraph.io/graphql'
variables = {'page': page}
r = post(URL, json={'query': query, 'variables': variables})
print(r.status_code)
print(r.content)
