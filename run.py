from scrapy.crawler import CrawlerProcess
from spider import SpiderGlobalTesting as Spider


def run_crawler():
    process = CrawlerProcess()
    process.crawl(Spider)
    process.start()
    return True


if __name__ == '__main__':
    run_crawler()
